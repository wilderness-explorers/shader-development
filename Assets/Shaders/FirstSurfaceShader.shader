﻿Shader "Holistic/Helloshader"
{
	Properties
	{
		_albedoColour ("Albedo Colour", Color) = (1, 1, 1, 1)
		//_emissionColour ("Emission Colour", Color) = (1, 1, 1, 1)
		//_normalColour ("Normal Colour", Color) = (1, 1, 1, 1)
	}

	SubShader
	{
		CGPROGRAM
			#pragma surface surf Lambert

			struct Input
			{
				float2 uvMainTex;
			};

			fixed4 _albedoColour;
			//fixed4 _emissionColour;
			//fixed4 _normalColour;

			void surf (Input IN, inout SurfaceOutput o)
			{
				o.Albedo = _albedoColour.rgb;
				//o.Emission = _emissionColour.rgb;
				//o.Normal = _normalColour.rgb;
			}

		ENDCG
	}

	FallBack "Diffuse"
}