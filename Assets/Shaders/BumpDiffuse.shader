﻿Shader "Holistic/BumpDiffuse"
{
    Properties
    {
        _myDiffuse ("Diffuse Texture", 2D) = "white" {}
		_myNormal ("Bump Texture", 2D) = "bump" {}
    }
    SubShader
    {
        CGPROGRAM
        #pragma surface surf Lambert

		sampler2D _myDiffuse;
		sampler2D _myNormal;

        struct Input
        {
            float2 uv_myDiffuse;
			float2 uv_myNormal;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
			o.Albedo = tex2D(_myDiffuse, IN.uv_myDiffuse).rgb;
			o.Normal = UnpackNormal(tex2D(_myNormal, IN.uv_myNormal));
        }
        ENDCG
    }
    FallBack "Diffuse"
}