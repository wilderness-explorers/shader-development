﻿Shader "Holistic/AllProps"
{
	Properties
	{
		//_myColour("Example Colour", Color) = (1, 1, 1, 1)
		//_myRange("Example Range", Range(0,5)) = 1
		//_myTex("Example Texture", 2D) = "white" {}
		//_myCube("Example Cube", CUBE) = "" {}
		//_myFloat("Example Float", Float) = 0.5
		//_myVector("example Vector", Vector) = (0.5,1,1,1)
		//_myTex3D("Example 3D Texture", 3D) = "white" {}
		
		_diffuseTex("Diffuse Texture", 2D) = "white" {}
		_emissiveTex("Emissive Texture", 2D) = "black" {}
	}

	SubShader
	{
		CGPROGRAM
		#pragma surface surf Lambert

		//fixed4 _myColour;
		//half _myRange;
		//sampler2D _myTex;
		//samplerCUBE _myCube;
		//float _myFloat;
		//float4 _myVector;

		sampler2D _diffuseTex;
		sampler2D _emissiveTex;

		struct Input
		{
			//float2 uv_myTex;
			//float3 worldRefl;

			float2 uv_diffuseTex;
			float2 uv_emissiveTex;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			//float4 color = float4(0, 1, 0, 1);
			//o.Albedo = (tex2D(_myTex, IN.uv_myTex) * color).rgb;
			
			//o.Albedo = (tex2D(_myTex, IN.uv_myTex) * _myRange * _myColour).rgb;
			//o.Albedo.g = 1;

			//o.Emission = texCUBE(_myCube, IN.worldRefl).rgb;

			o.Albedo = tex2D(_diffuseTex, IN.uv_diffuseTex).rgb;
			o.Emission = tex2D(_emissiveTex, IN.uv_emissiveTex).rgb;
		}

		ENDCG
	}
	FallBack "Diffuse"
}